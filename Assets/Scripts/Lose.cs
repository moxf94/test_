using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Lose : MonoBehaviour
{
    
   public static int loseValue;
    private int i;
    Text loseText;
    void Start()
    {
        loseText = GetComponent<Text>();
    }

    void Update()
    {
        loseText.text = "�� ������� 10!: " + loseValue;
        CountForLose();
        Restart();
    }

    void CountForLose()
    {
        GameObject[] obj = GameObject.FindGameObjectsWithTag("Enemy");

        loseValue = obj.Length;
    }

    void Restart()
    {
        if(loseValue == 10)
        {
            SceneManager.LoadScene("Restart");
        }
    }
}
