using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 public class Bonus : MonoBehaviour
{
    [SerializeField] private GameObject bon;


    private void Start()
    {
        
        StartCoroutine(Spawn());
       
    }

    private void Update()
    {
        
    }
    IEnumerator Spawn()
    {
        while (true)
        {
            yield return StartCoroutine(Wait());
            Instantiate(bon, new Vector3(Random.Range(-6.85f, 33f), 2.45f, Random.Range(-6.09f, 5.43f)), Quaternion.identity);
            yield return new WaitForSeconds(Random.Range(15f, 23f));
            
        }
    }
    
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(15.0f);
    }
}
