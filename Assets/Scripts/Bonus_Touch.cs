using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus_Touch : MonoBehaviour
{
    private void OnMouseDown()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Enemy");
        for (int i = 0; i < objs.Length; i++)
        {
            Destroy(objs[i]);
            Destroy(gameObject);
        }

    }
}
