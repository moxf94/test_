using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Touch : MonoBehaviour
{
    private void OnMouseDown()
    {
        Lose.loseValue --;
        Score.scoreValue += 1;
        Destroy(this.gameObject);
    }
}
