using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class health : MonoBehaviour
{
    [SerializeField]private int hp;
    
    public void GetDamage(int damage)

    {
        int health = hp - damage;
        if(health == 0)
        {
            Score.scoreValue += 2;
            Destroy(gameObject);
        }

        hp = health;
    }
    private void OnMouseDown()
    {
        
        GetDamage(1);
    }
}
